package bomdestino.sgm.georreferencia.repository;

import bomdestino.sgm.georreferencia.model.entity.BairroEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BairroRepository extends JpaRepository<BairroEntity, Long> {
}