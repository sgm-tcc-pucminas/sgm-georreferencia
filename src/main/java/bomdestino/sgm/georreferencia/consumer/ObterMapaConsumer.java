package bomdestino.sgm.georreferencia.consumer;

import bomdestino.sgm.georreferencia.model.message.ExecucaoServicoMessage;
import bomdestino.sgm.georreferencia.service.MapaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static java.lang.String.format;

@Component
public class ObterMapaConsumer {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MapaService service;

    @RabbitListener(queues = "georreferencia-obter-mapa")
    public void listenGroupFoo(ExecucaoServicoMessage message) {
        logger.info(format("Obtendo mapa para o CEP %s", message.getCep()));
        service.obterPorCep(message);
    }
}
