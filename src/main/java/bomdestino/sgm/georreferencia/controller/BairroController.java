package bomdestino.sgm.georreferencia.controller;

import bomdestino.sgm.georreferencia.model.dto.BairroDTO;
import bomdestino.sgm.georreferencia.model.dto.BairroResponseDTO;
import bomdestino.sgm.georreferencia.service.BairroService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static java.util.Objects.isNull;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/public/bairro")
public class BairroController {

    @Autowired
    private BairroService service;

    @ApiOperation(
            value = "Cadastro de Bairro",
            notes = "Inserção de novo cadastro de bairro na base de dados",
            response = BairroDTO.class
    )
    @PostMapping
    public ResponseEntity<BairroDTO> criar(@RequestBody final BairroDTO bairro) {
        BairroDTO bairroResult = service.criar(bairro);
        return ResponseEntity.ok().body(bairroResult);
    }

    @ApiOperation(
            value = "Obter todos os Bairros",
            notes = "Obtenção de todos os bairros cadastrados na base de dados",
            response = BairroResponseDTO.class
    )
    @GetMapping
    public ResponseEntity<BairroResponseDTO> obterTodos() {
        BairroResponseDTO bairroResponse = service.obter();

        if (isNull(bairroResponse)) {
            return ResponseEntity.noContent().build();

        } else {
            return ResponseEntity.ok().body(bairroResponse);
        }
    }

    @ApiOperation(
            value = "Obter Bairro por ID",
            notes = "Obtenção do bairro cadastrado com o ID informado",
            response = BairroDTO.class
    )
    @GetMapping("/{idBairro}")
    public ResponseEntity<BairroDTO> obterTodos(@PathVariable("idBairro") Long idBairro) {
        BairroDTO bairroDTO = service.obter(idBairro);

        if (isNull(bairroDTO)) {
            return ResponseEntity.noContent().build();

        } else {
            return ResponseEntity.ok().body(bairroDTO);
        }
    }
}