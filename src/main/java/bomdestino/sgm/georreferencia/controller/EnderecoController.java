package bomdestino.sgm.georreferencia.controller;

import bomdestino.sgm.georreferencia.model.dto.EnderecoRequestDTO;
import bomdestino.sgm.georreferencia.model.dto.EnderecoResponseDTO;
import bomdestino.sgm.georreferencia.model.dto.EnderecoResponseListaDTO;
import bomdestino.sgm.georreferencia.service.EnderecoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static java.util.Objects.isNull;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/public/endereco")
public class EnderecoController {

    @Autowired
    private EnderecoService service;

    @ApiOperation(
            value = "Cadastro de Endereço",
            notes = "Inserção de novo cadastro de endereço na base de dados",
            response = EnderecoResponseDTO.class
    )
    @PostMapping
    public ResponseEntity<EnderecoResponseDTO> criar(@RequestBody final EnderecoRequestDTO endereco) {
        EnderecoResponseDTO enderecoResponse = service.criar(endereco);
        return ResponseEntity.ok().body(enderecoResponse);
    }

    @ApiOperation(
            value = "Obter todos os Endereços",
            notes = "Obtenção de todos os endereços cadastrados na base de dados",
            response = EnderecoResponseListaDTO.class
    )
    @GetMapping
    public ResponseEntity<EnderecoResponseListaDTO> obterTodos() {
        EnderecoResponseListaDTO enderecoResponse = service.obterTodos();

        if (isNull(enderecoResponse)) {
            return ResponseEntity.noContent().build();

        } else {
            return ResponseEntity.ok().body(enderecoResponse);
        }
    }

    @ApiOperation(
            value = "Obter Endereço por CEP",
            notes = "Obtenção do endereço cadastrado com o CEP informado",
            response = EnderecoResponseDTO.class
    )
    @GetMapping("/{cep}")
    public ResponseEntity<EnderecoResponseDTO> obter(@PathVariable("cep") Integer cep) {
        EnderecoResponseDTO endereco = service.obter(cep);

        if (isNull(endereco)) {
            return ResponseEntity.noContent().build();

        } else {
            return ResponseEntity.ok().body(endereco);
        }
    }
}