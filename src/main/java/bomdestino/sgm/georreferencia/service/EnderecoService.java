package bomdestino.sgm.georreferencia.service;

import bomdestino.sgm.georreferencia.model.dto.EnderecoRequestDTO;
import bomdestino.sgm.georreferencia.model.dto.EnderecoResponseDTO;
import bomdestino.sgm.georreferencia.model.dto.EnderecoResponseListaDTO;
import bomdestino.sgm.georreferencia.model.entity.BairroEntity;
import bomdestino.sgm.georreferencia.model.entity.EnderecoEntity;
import bomdestino.sgm.georreferencia.repository.EnderecoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

@Service
public class EnderecoService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private EnderecoRepository repository;

    public EnderecoResponseDTO criar(EnderecoRequestDTO endereco) {
        EnderecoEntity enderecoRequest = new EnderecoEntity()
                .setCep(endereco.getCep())
                .setPrefixo(endereco.getPrefixo())
                .setLogradouro(endereco.getLogradouro())
                .setBairroInicio(new BairroEntity().setId(endereco.getIdBairroInicio()))
                .setBairroFim(new BairroEntity().setId(endereco.getIdBairroFim()))
                .setImagemUrl("http://34.95.219.145:9000/compartilhado/share_mapa/mapa.jpg");

        EnderecoEntity enderecoResult = repository.save(enderecoRequest);

        return new EnderecoResponseDTO()
                .setCep(enderecoResult.getCep())
                .setPrefixo(enderecoResult.getPrefixo())
                .setLogradouro(enderecoResult.getLogradouro())
                .setImagemUrl(enderecoResult.getImagemUrl())
                .setBairroInicio(enderecoResult.getBairroInicio().getNome())
                .setBairroFim(enderecoResult.getBairroFim().getNome());
    }

    public EnderecoResponseDTO obter(Integer cep) {
        logger.info(format("Obtendo endereço por CEP %d", cep));
        Optional<EnderecoEntity> enderecoOpt = repository.findById(cep);

        if (enderecoOpt.isPresent()) {
            EnderecoEntity endereco = enderecoOpt.get();

            return new EnderecoResponseDTO()
                    .setCep(endereco.getCep())
                    .setPrefixo(endereco.getPrefixo())
                    .setLogradouro(endereco.getLogradouro())
                    .setImagemUrl(endereco.getImagemUrl())
                    .setBairroInicio(endereco.getBairroInicio().getNome())
                    .setBairroFim(endereco.getBairroFim().getNome());

        } else {
            return new EnderecoResponseDTO()
                    .setCep(cep)
                    .setPrefixo("Avenida")
                    .setLogradouro("Default")
                    .setImagemUrl("http://34.95.219.145:9000/compartilhado/share_mapa/mapa.jpg")
                    .setBairroInicio("Centro")
                    .setBairroFim("Centro");
        }
    }

    public EnderecoResponseListaDTO obterTodos() {
        List<EnderecoEntity> enderecos = repository.findAll();

        if (enderecos.isEmpty()) {
            return null;

        } else {
            return new EnderecoResponseListaDTO(enderecos.stream()
                    .map(endereco -> new EnderecoResponseDTO()
                            .setCep(endereco.getCep())
                            .setPrefixo(endereco.getPrefixo())
                            .setLogradouro(endereco.getLogradouro())
                            .setImagemUrl(endereco.getImagemUrl())
                            .setBairroInicio(endereco.getBairroInicio().getNome())
                            .setBairroFim(endereco.getBairroFim().getNome()))
                    .collect(toList()));
        }
    }
}