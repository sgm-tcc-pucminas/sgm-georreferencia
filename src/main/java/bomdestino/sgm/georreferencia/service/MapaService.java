package bomdestino.sgm.georreferencia.service;

import bomdestino.sgm.georreferencia.model.dto.EnderecoResponseDTO;
import bomdestino.sgm.georreferencia.model.message.ExecucaoServicoMessage;
import bomdestino.sgm.georreferencia.producer.MapaServicoProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MapaService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MapaServicoProducer producer;

    @Autowired
    private EnderecoService enderecoService;

    public void obterPorCep(ExecucaoServicoMessage servicoMessage) {
        EnderecoResponseDTO endereco = enderecoService.obter(servicoMessage.getCep());
        servicoMessage.setUrlMapa(endereco.getImagemUrl());

        StringBuilder mensagem = new StringBuilder();
        mensagem.append("\n\n [*** SGM GEORREFERENCIA ***] ---------------------------------------------------------");
        mensagem.append("\n\n cep: " + endereco.getCep());
        mensagem.append("\n\n mapa: " + endereco.getImagemUrl());
        mensagem.append("\n\n[*** SGM GEORREFERENCIA ***] ---------------------------------------------------------\n\n");

        logger.info(mensagem.toString());
        producer.sendMessage(servicoMessage);
    }
}