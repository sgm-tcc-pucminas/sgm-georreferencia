package bomdestino.sgm.georreferencia.service;

import bomdestino.sgm.georreferencia.model.dto.BairroDTO;
import bomdestino.sgm.georreferencia.model.dto.BairroResponseDTO;
import bomdestino.sgm.georreferencia.model.entity.BairroEntity;
import bomdestino.sgm.georreferencia.repository.BairroRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.lang.String.format;
import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;

@Service
public class BairroService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private BairroRepository repository;

    public BairroDTO obter(Long idBairro) {
        logger.info(format("Obtendo bairro ID %d", idBairro));
        BairroEntity bairro = repository.findById(idBairro).orElse(null);

        if (isNull(bairro)) {
            return null;

        } else {
            return new BairroDTO()
                    .setId(bairro.getId())
                    .setNome(bairro.getNome());
        }
    }

    public BairroDTO criar(BairroDTO bairro) {
        logger.info(format("Cadastrando bairro ID %s", bairro.getNome()));
        BairroEntity result = repository.save(new BairroEntity()
                .setId(bairro.getId())
                .setNome(bairro.getNome()));

        return new BairroDTO()
                .setId(result.getId())
                .setNome(result.getNome());
    }

    public BairroResponseDTO obter() {
        logger.info("Obtendo todos os bairros cadastrados");
        List<BairroEntity> bairroLista = repository.findAll();

        if (bairroLista.isEmpty()) {
            return null;

        } else {
            return new BairroResponseDTO(bairroLista.stream()
                    .map(bairro -> new BairroDTO()
                            .setId(bairro.getId())
                            .setNome(bairro.getNome()))
                    .collect(toList()));
        }
    }
}