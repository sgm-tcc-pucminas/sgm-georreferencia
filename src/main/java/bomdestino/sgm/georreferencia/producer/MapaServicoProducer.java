package bomdestino.sgm.georreferencia.producer;

import bomdestino.sgm.georreferencia.model.message.ExecucaoServicoMessage;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MapaServicoProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendMessage(ExecucaoServicoMessage message) {
        rabbitTemplate.convertAndSend("integracao-executar-servico", message);
    }
}
