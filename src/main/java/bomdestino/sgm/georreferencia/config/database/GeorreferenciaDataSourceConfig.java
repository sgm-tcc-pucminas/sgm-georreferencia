package bomdestino.sgm.georreferencia.config.database;

import net.javacrumbs.shedlock.core.LockProvider;
import net.javacrumbs.shedlock.provider.jdbctemplate.JdbcTemplateLockProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(
        entityManagerFactoryRef = "georreferenciaEntityManagerFactory",
        transactionManagerRef = "georreferenciaTransactionManager",
        basePackages = "bomdestino.sgm.georreferencia.repository"
)
@EnableTransactionManagement
public class GeorreferenciaDataSourceConfig {

    @Value("${spring.config.datasource.username}")
    private String username;

    @Value("${spring.config.datasource.password}")
    private String password;

    @Value("${spring.config.datasource.databases.georreferencia.url}")
    private String url;

    @Value("${spring.config.datasource.databases.georreferencia.driver-class-name}")
    private String driver;

    @Autowired
    private JpaVendorAdapter jpaVendorAdapter;

    @Bean
    public LockProvider lockProvider() {
        DriverManagerDataSource driverManager = new DriverManagerDataSource();
        driverManager.setDriverClassName(driver);
        driverManager.setUrl(url);
        driverManager.setUsername(username);
        driverManager.setPassword(password);
        return new JdbcTemplateLockProvider(driverManager);
    }

    @Bean(name = "georreferencia-db")
    public DataSource georreferenciaDataSource() {
        DriverManagerDataSource driverManager = new DriverManagerDataSource();
        driverManager.setDriverClassName(driver);
        driverManager.setUrl(url);
        driverManager.setUsername(username);
        driverManager.setPassword(password);
        return driverManager;
    }

    @Primary
    @Bean(name = "georreferenciaEntityManagerFactory")
    public EntityManagerFactory georreferenciaEntityManagerFactory(final @Qualifier("georreferencia-db") DataSource georreferenciaDataSource) {
        LocalContainerEntityManagerFactoryBean lef = new LocalContainerEntityManagerFactoryBean();
        lef.setDataSource(georreferenciaDataSource);
        lef.setJpaVendorAdapter(jpaVendorAdapter);
        lef.setJpaProperties(JpaProperties.additional());
        lef.setPackagesToScan("bomdestino.sgm.georreferencia.model.entity");
        lef.setPersistenceUnitName("georreferenciaDb");
        lef.afterPropertiesSet();
        return lef.getObject();
    }

    @Bean(name = "georreferenciaTransactionManager")
    public PlatformTransactionManager georreferenciaTransactionManager(@Qualifier("georreferenciaEntityManagerFactory") EntityManagerFactory georreferenciaEntityManagerFactory) {
        return new JpaTransactionManager(georreferenciaEntityManagerFactory);
    }
}
