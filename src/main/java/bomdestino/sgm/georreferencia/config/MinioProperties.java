package bomdestino.sgm.georreferencia.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "spring.minio")
public class MinioProperties {

    private String urlMinio;
    private String accessKey;
    private String secretKey;
    private String prefix_share_files;
    private String prefix_private_files;

    public String getUrlMinio() {
        return urlMinio;
    }

    public void setUrlMinio(String urlMinio) {
        this.urlMinio = urlMinio;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getPrefix_share_files() {
        return prefix_share_files;
    }

    public void setPrefix_share_files(String prefix_share_files) {
        this.prefix_share_files = prefix_share_files;
    }

    public String getPrefix_private_files() {
        return prefix_private_files;
    }

    public void setPrefix_private_files(String prefix_private_files) {
        this.prefix_private_files = prefix_private_files;
    }
}
