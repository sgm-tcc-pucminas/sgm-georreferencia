package bomdestino.sgm.georreferencia.config.mensageria;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableRabbit
public class RabbitMQConfig {

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public Queue georreferenciaObterMapaQueue() {
        return new Queue("georreferencia-obter-mapa", true, false, false);
    }

    @Bean
    FanoutExchange georreferenciaObterMapaExchange() {
        return new FanoutExchange("georreferencia-obter-mapa");
    }

    @Bean
    Binding georreferenciaObterMapaBinding(Queue georreferenciaObterMapaQueue,
                                           FanoutExchange georreferenciaObterMapaExchange) {

        return BindingBuilder.bind(georreferenciaObterMapaQueue).to(georreferenciaObterMapaExchange);
    }
}
