package bomdestino.sgm.georreferencia.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static java.util.Objects.isNull;


@Configuration
@EnableConfigurationProperties(MinioProperties.class)
public class MinioConfig {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MinioProperties minioProperties;

    @Bean
    public io.minio.MinioClient getMinioClient() {
        io.minio.MinioClient minioClient = null;

        if (isNull(minioClient)) {
            try {
                minioClient = new io.minio.MinioClient(
                        minioProperties.getUrlMinio(),
                        minioProperties.getAccessKey(),
                        minioProperties.getSecretKey());

            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }

        return minioClient;
    }
}

