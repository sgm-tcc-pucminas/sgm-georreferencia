package bomdestino.sgm.georreferencia;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

@SpringBootApplication
public class SgmGeorreferenciaApplication {

	private static final Logger log = LoggerFactory.getLogger(SgmGeorreferenciaApplication.class);

	public static void main(String[] args) {
		ConfigurableApplicationContext run = SpringApplication.run(SgmGeorreferenciaApplication.class, args);
		ConfigurableEnvironment environment = run.getEnvironment();

		log.info("\n\n\t------------------------------------------------------------\n\t" +
				":: SGM Georreferencia inicializado na porta: " + environment.getProperty("server.port") + " ::" +
				"\n\t------------------------------------------------------------\n\t");
	}

}
