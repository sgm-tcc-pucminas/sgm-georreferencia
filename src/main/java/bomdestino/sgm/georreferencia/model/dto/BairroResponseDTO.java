package bomdestino.sgm.georreferencia.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BairroResponseDTO implements Serializable {

    @JsonProperty("bairros")
    private List<BairroDTO> bairros;

    public BairroResponseDTO() {
    }

    public BairroResponseDTO(List<BairroDTO> bairros) {
        this.bairros = bairros;
    }

    public List<BairroDTO> getBairros() {
        return bairros;
    }

    public BairroResponseDTO setBairros(List<BairroDTO> bairros) {
        this.bairros = bairros;
        return this;
    }
}