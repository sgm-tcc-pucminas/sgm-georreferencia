package bomdestino.sgm.georreferencia.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EnderecoResponseListaDTO implements Serializable {

    @JsonProperty("enderecolista")
    private List<EnderecoResponseDTO> enderecoLista;

    public EnderecoResponseListaDTO() {
    }

    public EnderecoResponseListaDTO(List<EnderecoResponseDTO> enderecoLista) {
        this.enderecoLista = enderecoLista;
    }

    public List<EnderecoResponseDTO> getEnderecoLista() {
        return enderecoLista;
    }

    public EnderecoResponseListaDTO setEnderecoLista(List<EnderecoResponseDTO> enderecoLista) {
        this.enderecoLista = enderecoLista;
        return this;
    }
}