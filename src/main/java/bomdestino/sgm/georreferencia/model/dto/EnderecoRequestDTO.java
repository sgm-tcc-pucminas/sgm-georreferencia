package bomdestino.sgm.georreferencia.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EnderecoRequestDTO implements Serializable {

    @JsonProperty("cep")
    private Integer cep;
    @JsonProperty("prefixo")
    private String prefixo;
    @JsonProperty("logradouro")
    private String logradouro;
    @JsonProperty("idbairroinicio")
    private Long idBairroInicio;
    @JsonProperty("idbairrofim")
    private Long idBairroFim;
    @JsonProperty("imagemurl")
    private String imagemUrl;

    public Integer getCep() {
        return cep;
    }

    public EnderecoRequestDTO setCep(Integer cep) {
        this.cep = cep;
        return this;
    }

    public String getPrefixo() {
        return prefixo;
    }

    public EnderecoRequestDTO setPrefixo(String prefixo) {
        this.prefixo = prefixo;
        return this;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public EnderecoRequestDTO setLogradouro(String logradouro) {
        this.logradouro = logradouro;
        return this;
    }

    public Long getIdBairroInicio() {
        return idBairroInicio;
    }

    public EnderecoRequestDTO setIdBairroInicio(Long idBairroInicio) {
        this.idBairroInicio = idBairroInicio;
        return this;
    }

    public Long getIdBairroFim() {
        return idBairroFim;
    }

    public EnderecoRequestDTO setIdBairroFim(Long idBairroFim) {
        this.idBairroFim = idBairroFim;
        return this;
    }

    public String getImagemUrl() {
        return imagemUrl;
    }

    public EnderecoRequestDTO setImagemUrl(String imagemUrl) {
        this.imagemUrl = imagemUrl;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EnderecoRequestDTO that = (EnderecoRequestDTO) o;
        return Objects.equals(cep, that.cep);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cep);
    }
}