package bomdestino.sgm.georreferencia.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EnderecoResponseDTO implements Serializable {

    @JsonProperty("cep")
    private Integer cep;
    @JsonProperty("prefixo")
    private String prefixo;
    @JsonProperty("logradouro")
    private String logradouro;
    @JsonProperty("bairroinicio")
    private String bairroInicio;
    @JsonProperty("bairrofim")
    private String bairroFim;
    @JsonProperty("imagemurl")
    private String imagemUrl;

    public Integer getCep() {
        return cep;
    }

    public EnderecoResponseDTO setCep(Integer cep) {
        this.cep = cep;
        return this;
    }

    public String getPrefixo() {
        return prefixo;
    }

    public EnderecoResponseDTO setPrefixo(String prefixo) {
        this.prefixo = prefixo;
        return this;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public EnderecoResponseDTO setLogradouro(String logradouro) {
        this.logradouro = logradouro;
        return this;
    }

    public String getBairroInicio() {
        return bairroInicio;
    }

    public EnderecoResponseDTO setBairroInicio(String bairroInicio) {
        this.bairroInicio = bairroInicio;
        return this;
    }

    public String getBairroFim() {
        return bairroFim;
    }

    public EnderecoResponseDTO setBairroFim(String bairroFim) {
        this.bairroFim = bairroFim;
        return this;
    }

    public String getImagemUrl() {
        return imagemUrl;
    }

    public EnderecoResponseDTO setImagemUrl(String imagemUrl) {
        this.imagemUrl = imagemUrl;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EnderecoResponseDTO that = (EnderecoResponseDTO) o;
        return Objects.equals(cep, that.cep);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cep);
    }
}