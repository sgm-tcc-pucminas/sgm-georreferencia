package bomdestino.sgm.georreferencia.model.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

@Entity(name = "endereco")
public class EnderecoEntity implements Serializable {

    @Id
    private Integer cep;
    private String prefixo;
    private String logradouro;
    private String imagemUrl;

    @ManyToOne
    @JoinColumn(name="idbairroinicio")
    private BairroEntity bairroInicio;
    @ManyToOne
    @JoinColumn(name="idbairrofim")
    private BairroEntity bairroFim;

    public Integer getCep() {
        return cep;
    }

    public EnderecoEntity setCep(Integer cep) {
        this.cep = cep;
        return this;
    }

    public String getPrefixo() {
        return prefixo;
    }

    public EnderecoEntity setPrefixo(String prefixo) {
        this.prefixo = prefixo;
        return this;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public EnderecoEntity setLogradouro(String logradouro) {
        this.logradouro = logradouro;
        return this;
    }

    public BairroEntity getBairroInicio() {
        return bairroInicio;
    }

    public EnderecoEntity setBairroInicio(BairroEntity bairroInicio) {
        this.bairroInicio = bairroInicio;
        return this;
    }

    public BairroEntity getBairroFim() {
        return bairroFim;
    }

    public EnderecoEntity setBairroFim(BairroEntity bairroFim) {
        this.bairroFim = bairroFim;
        return this;
    }

    public String getImagemUrl() {
        return imagemUrl;
    }

    public EnderecoEntity setImagemUrl(String imagemUrl) {
        this.imagemUrl = imagemUrl;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EnderecoEntity that = (EnderecoEntity) o;
        return Objects.equals(cep, that.cep);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cep);
    }
}